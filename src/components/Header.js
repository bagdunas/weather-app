import React from 'react'

// SIMPLE FUNCTION COMPONENT

const header = ( props ) => {
  return (
    <div>
      <h2>{props.title}</h2>
      {props.children ? <p>{props.children}</p> : ''}
    </div>
  )
};

export default header;