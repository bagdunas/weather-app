import React, { Component } from 'react';
import { convertKelvinToCelsius } from '../helpers/helpers';

export default class InfoBox extends Component {

  generateItemList() {
    return this.props.activeItem.map( ( item ) => {
      const date = new Date( item.dt * 1000 );
      return <li className="list-group-item" key={item.dt}>
              <span>Godzina: {(date.getHours() < 9 ? '0' + date.getHours() : date.getHours()) + ':00'}
                <span> Średnia: {convertKelvinToCelsius( item.main.temp )} </span>
              </span>
      </li>
    } );
  }

  render() {
    return (
      <div className="card">
        <div className="card-body">
          <h5 className="card-title">Pogoda na dzięń: {this.props.activeKey}</h5>
          <ul className="ist-group">
            {this.generateItemList()}
          </ul>
        </div>
      </div>
    );
  }
}
