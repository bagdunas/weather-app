import React from 'react'

// SIMPLE FUNCTION COMPONENT

const WeatherInfo = ( props ) => {
  console.log(props);
  return (
    <section className="weather-info">
      <div className="weather-info_header">
        <span className="weather-info_city">{props.city.name}</span>
        <span className="weather-info_country">{props.city.country}</span>
      </div>
      <div className="weather-info_content">
        <span className="weather-info_temperature">722*</span>
        <span className="weather-info_humidity">85%</span>
        <span className="weather-info_pressure">968.38 PSI</span>
        <span className="weather-info_wind">3.57</span>
        <span className="weather-info_clouds">0</span>
      </div>
      <div className="weather-info_description">Clear sky</div>
    </section>
  )
};

export default WeatherInfo;