import React, { Component } from 'react';

export default class ListNav extends Component {

  swichDateHandler( key ) {
    if ( this.props.activeKey !== key ) {
      this.props.onClick( key )
    }
  }

  generateItemList() {
    return Object.keys( this.props.items ).map( ( key ) => {
      return <li className="nav-item"
                 key={key}
                 onClick={this.swichDateHandler.bind( this, key )}>
        <span className="nav-link">{key}</span>
      </li>
    } );
  }

  render() {
    if ( !this.props.items ) {
      return <p>List items are not defined!</p>
    }

    const list = this.generateItemList();
    return (
      <ul className="nav justify-content-center">
        {list}
      </ul>
    );
  }
}
