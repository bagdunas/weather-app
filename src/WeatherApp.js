import React, { Component } from 'react';
import logo from './logo.svg';

import { forecast } from './forecast.js';
import ListNav from './components/listNav';
import InfoBox from './components/infoBox';
import Header from './components/Header'; //Simple function component
import WeatherInfo from './components/WeatherInfo/WeatherInfo'; //Simple function component

class App extends Component {
  elementList = null;
  state = {
    activeKey: null
  };

  // componentWillMount(){
  //   this.setState({ activeKey: null });
  // }

  prepareDaysObject() {  // Convert Array to Object with unique (day) keys
    let daysWeather = {};
    // eslint-disable-next-line
    forecast.list.map( ( el ) => {
      const day = new Date( el.dt * 1000 ).getDate();
      (daysWeather[ day ] = daysWeather[ day ] || []).push( el )
    } );
    return daysWeather;
  }

  render() {
    this.elementList = this.prepareDaysObject();
    return (
      <div className="WeatherApp">
        <Header title="Some text"/>
        <header className="WeatherApp-header">
          <img src={logo} className="WeatherApp-logo" alt="logo"/>
        </header>
        <nav className="l-margin">
          <ListNav
            activeKey={this.state.activeKey}
            items={this.elementList}
            onClick={el => {
              this.setState( { activeKey: el } );
            }}>
          </ListNav>
        </nav>
        <div className="row justify-content-center l-margin">
          <WeatherInfo city={forecast.city}/>
          <div className="col-sm-6">
            {this.state.activeKey ?
              <InfoBox activeKey={this.state.activeKey} activeItem={this.elementList[ this.state.activeKey ]}></InfoBox>
              : ''}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
