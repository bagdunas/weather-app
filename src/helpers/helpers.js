export const convertKelvinToCelsius = (value) => {
  return Math.round(value - 273.15)
};